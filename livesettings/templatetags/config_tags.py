from django import template
from django.conf import settings
from django.contrib.sites.models import Site
from django.core import urlresolvers
from livesettings import config_value
from livesettings.utils import url_join
import logging
from django.template import Node

log = logging.getLogger('configuration.config_tags')

register = template.Library()


def force_space(value, chars=40):
    """Forces spaces every `chars` in value"""
    chars = int(chars)
    if len(value) < chars:
        return value
    else:
        out = []
        start = 0
        end = 0
        looping = True

        while looping:
            start = end
            end += chars
            out.append(value[start:end])
            looping = end < len(value)

    return ' '.join(out)


def break_at(value, chars=40):
    """Force spaces into long lines which don't have spaces"""

    chars = int(chars)
    value = unicode(value)
    if len(value) < chars:
        return value
    else:
        out = []
        line = value.split(' ')
        for word in line:
            if len(word) > chars:
                out.append(force_space(word, chars))
            else:
                out.append(word)

    return " ".join(out)


register.filter('break_at', break_at)


def config_boolean(option):
    """Looks up the configuration option, returning true or false."""
    args = option.split('.')
    try:
        val = config_value(*args)
    except:
        log.warn('config_boolean tag: Tried to look up config setting "%s", got SettingNotSet, returning False', option)
        val = False
    if val:
        return "true"
    else:
        return ""


register.filter('config_boolean', config_boolean)


def admin_site_views(view):
    """Returns a formatted list of sites, rendering for view, if any"""

    if view:
        path = urlresolvers.reverse(view)
    else:
        path = None

    links = []
    for site in Site.objects.all():
        paths = ["http://", site.domain]
        if path:
            paths.append(path)

        links.append((site.name, url_join(paths)))

    ret = {
        'links': links,
    }
    return ret


register.inclusion_tag('livesettings/_admin_site_views.html')(admin_site_views)
from django.template.base import render_value_in_context


class GetSettingValue(Node):
    """
    Template node class used by ``get_setting_value``.
    """

    def __init__(self, group, setting, variable=None):
        self.group = group
        self.setting = setting
        self.variable = variable

    def render(self, context):
        #output = self.filter_expression.resolve(context)
        if self.variable:
            context[self.variable] = config_value(self.group, self.setting)
            value = ""
        else:
            value = render_value_in_context(config_value(self.group, self.setting), context)

        return value

def do_get_setting_value(parser, token):
    """
    Retrieves the settings value.
    Syntax:
        {% get_setting_value [group] [setting_name] as [varname] %}
        or
        {% get_setting_value [group] [setting_name] %}

	Example::
        {% get_setting_value social_settings FACEBOOK_URL as FACEBOOK_URL %}
        or
        {% get_setting_value contact_settings CONTACT_PHONE %}
    """

    bits = token.contents.split()
    if len(bits) != 3 and len(bits) != 5:
        raise template.TemplateSyntaxError("'%s' tag takes two or four arguments" % bits[0])
    if len(bits) == 5:
        if bits[3] != 'as':
            raise template.TemplateSyntaxError("second argument to '%s' tag must be 'as'" % bits[0])
    return GetSettingValue(bits[1], bits[2], bits[4] if len(bits) == 5 else None)

register.tag("get_setting_value", do_get_setting_value)
